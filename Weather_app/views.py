from distutils.log import error
from email import message
from multiprocessing import context
from django import forms
import requests
from django.shortcuts import redirect, render
from .models import City
from .forms import Cityform
from django.http import HttpResponse


# Create your views here.


def index(request):
    
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=b352f8fc3cb8bac5e22eca8400559c66'
    cities = City.objects.all()
    error_message=""

    if request.method == "POST":
        
        if 'add_city' in request.POST:
            form = Cityform(request.POST)
            city2=form['name'].value()
            for city in cities:
                if city2==city.name:
                    return redirect('index')
            r = requests.get(url.format(city2)).json()
            if r['cod']==200:
                form.save()
            else:
                
                # error_message="city is not found"
                #error_message = HttpResponse("city not found")
                #return HttpResponse("city not found")
                return redirect('index')          

    form = Cityform()
    cities = City.objects.all()


    weather_data = []
    error_data={}
    error=[]

    for city in cities:
        r = requests.get(url.format(city)).json()
        

       
        if r['cod']==200:
            city_weather ={
                'city': city.name,
                'temprature': r['main']['temp'],
                'description': r['weather'][0]['description'],
                'icon': r['weather'][0]['icon'],
                'cod':r['cod']
            }
        
            weather_data.append(city_weather)
            
        # else:
        #     error_data={
        #         'city1':city.name,
        #         'cod':r['cod'],
        #         'message': r['message'],
        #     }
        #     error.append(error_data)

    context = {'weather_data': weather_data, 'form': form,'error_data':error,'error_message':error_message}
    return render(request, 'Weather_app/index.html', context)

def Viewcity(request,name):
    #data=request.POST.get(name)
    data = City.objects.get(name=name)
    url = 'http://api.openweathermap.org/data/2.5/weather?q={}&units=metric&appid=b352f8fc3cb8bac5e22eca8400559c66'
    r = requests.get(url.format(data)).json()
    weather_data=[]
    print(r)
    city_weather = {
                'city': r['name'],
                'temprature': r['main']['temp'],
                'description': r['weather'][0]['description'],
                'icon': r['weather'][0]['icon'],
                'cod':r['cod'],
                'temp_max':r['main']['temp_max'],
                'temp_min':r['main']['temp_min'],
                'feels_like':r['main']['feels_like'],
                'visibility':r['visibility'],
                'wind_speed':r['wind']['speed'],
                'humidity':r['main']['humidity'],
            }
    print(city_weather)
    weather_data.append(city_weather)
    context={'weather_data': weather_data }
    return render(request,'Weather_app/cityview.html',context)
    
    



def Deletecity(request,name):
    if request.method =="POST":
        if 'delete_city' in request.POST:
            data=City.objects.get(name=name)
            data.delete()
            return redirect('index')
